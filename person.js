class Person{


    constructor(name,age,salary,sex){
        this.name=name;
        this.age=age;
        this.salary=salary;
        this.sex=sex;
      }
  

      static sorting(arrPerson,field,order) {
        let arrad=[...arrPerson];
        
        if (arrad.length <= 1) {
            return arrad;
          }
        
          let pivot = arrad[0][field];
          let pivotItems=arrad[0];
          

          let left = []; 
          let right = [];
        

          for (let i = 1; i < arrad.length; i++) {
              if(order==='asc')
              {arrad[i][field] < pivot ? left.push(arrad[i]) : right.push(arrad[i]);}
              else if(order==='desc')
              {arrad[i][field] < pivot ? right.push(arrad[i]) : left.push(arrad[i]);}
          }
        
          return Person.sorting(left,field,order).concat(pivotItems, Person.sorting(right,field,order));

    }   

}


const person1=new Person("sahdev",19,12000,"M");
const person2=new Person("ankit",20,12000,"M");
const person3=new Person("nakul",29,40000,"M");
const person4=new Person("urvashi",25,120000,"F");
const person5=new Person("ankita",24,51000,"F");

const arrPerson=[person1,person2,person3,person4,person5];

console.log(Person.sorting(arrPerson,'name','asc'));

